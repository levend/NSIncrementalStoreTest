//
//  ViewController.swift
//  IncrementaStoreTest
//
//  Created by balazsgollner on 19/10/15.
//  Copyright © 2015 EPAM. All rights reserved.
//

import UIKit

class ItemListViewController : UIViewController {
    
    @IBOutlet weak var tableView: UITableView!
    let cdHelper = CoreDataHelper()
    var items = [MagicUnicorn]()

    override func viewDidLoad() {
        super.viewDidLoad()

        self.refreshData()
    }
    
    @IBAction func addNewItemPressed(sender: AnyObject) {
        self.addNewUnicorn()
    }
    
    @IBAction func refreshPressed(sender: AnyObject) {
        self.refreshData()
    }
    
    private func refreshData() {
        
        dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0)) { () -> Void in
            
            self.items = MagicUnicorn.findAllInContext(self.cdHelper.managedObjectContext)
            
            dispatch_async(dispatch_get_main_queue(), { () -> Void in
                self.tableView.reloadData()
            })
        }
    }
    
    private func addNewUnicorn() {
        let newUnicorn : MagicUnicorn = MagicUnicorn.createEntityInContext(self.cdHelper.managedObjectContext)
        
        newUnicorn.name = "Moszi"
        newUnicorn.cornLength = 2
        
        self.cdHelper.saveContext()
    }
}

extension ItemListViewController : UITableViewDataSource {
    
    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return items.count
    }
    
    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCellWithIdentifier("DisplayCell", forIndexPath: indexPath)
        
        let currentItem = items[indexPath.row]
        cell.textLabel?.text = "Name: \(currentItem.name!)"
        cell.detailTextLabel?.text = "Corn length: \(currentItem.cornLength!)"
        
        return cell
    }
}

