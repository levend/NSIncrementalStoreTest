//
//  NSManagedObject-HelperExtensions.swift
//  IncrementaStoreTest
//
//  Created by balazsgollner on 20/10/15.
//  Copyright © 2015 EPAM. All rights reserved.
//

import Foundation
import CoreData

extension NSManagedObject {
    
    class func createEntityInContext<T: NSManagedObject>(context:NSManagedObjectContext) -> T {
        let className = NSStringFromClass(T.self).componentsSeparatedByString(".").last!
        let entity = NSEntityDescription.insertNewObjectForEntityForName(className, inManagedObjectContext: context);
        return entity as! T
    }
    
    class func findAllInContext<T: NSManagedObject>(context:NSManagedObjectContext)->[T] {
        let className = NSStringFromClass(T.self).componentsSeparatedByString(".").last!
        let fetchRequest = NSFetchRequest(entityName: className)
        
        do {
            let result = try context.executeFetchRequest(fetchRequest)
            return result as! [T]
        } catch let error as NSError {
            print("Fetch failed: \(error.localizedDescription)")
            return []
        }
    }
    
    func JSONRepresentation() -> String {
        
        //get the properties
        let json = NSMutableString();
        json.appendString("{")
        self.entity.properties.forEach { (currentProperty:NSPropertyDescription) -> () in
            
            if let attribute = currentProperty as? NSAttributeDescription {
                let currentPropertyName = attribute.name
                let c : AnyClass? = NSClassFromString(attribute.attributeValueClassName!)
                
                if let _ = c as? NSNumber.Type {
                    let currentValue = self.valueForKey(currentPropertyName) as! NSNumber
                    json.appendString("\n\"\(currentPropertyName)\" : \(currentValue),")
                }
                
                if let _ = c as? NSString.Type {
                    let currentValue = self.valueForKey(currentPropertyName) as! NSString
                    json.appendString("\n\"\(currentPropertyName)\" : \"\(currentValue)\",")
                }
            }
            
        }
        
        //delete the last comma
        json.deleteCharactersInRange(NSMakeRange(json.length-1, 1))
        
        json.appendString("\n}")
        return String(json)
    }
    
    func attributes() -> [NSAttributeDescription] {
        
        return self.entity.attributes()
    }
}

extension NSEntityDescription {
    
    func attributes() -> [NSAttributeDescription] {
        
        var array = [NSAttributeDescription]()
        
        self.properties.forEach { (currentProperty:NSPropertyDescription) -> () in
            
            if let attribute = currentProperty as? NSAttributeDescription {
                array.append(attribute)
            }
        }
        
        return array
    }
    
}