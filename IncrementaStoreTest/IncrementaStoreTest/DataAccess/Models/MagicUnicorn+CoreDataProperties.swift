//
//  MagicUnicorn+CoreDataProperties.swift
//  
//
//  Created by balazsgollner on 20/10/15.
//
//
//  Choose "Create NSManagedObject Subclass…" from the Core Data editor menu
//  to delete and recreate this implementation file for your updated model.
//

import Foundation
import CoreData

extension MagicUnicorn {

    @NSManaged var name: String?
    @NSManaged var cornLength: NSNumber?

}
