//
//  RESTIncrementalStore.swift
//  IncrementaStoreTest
//
//  Created by balazsgollner on 19/10/15.
//  Copyright © 2015 EPAM. All rights reserved.
//

import UIKit
import CoreData
import Alamofire

class RESTIncrementalStore: NSIncrementalStore {
    
    override class func initialize() {
        NSPersistentStoreCoordinator.registerStoreClass(self, forStoreType:self.storeType)
    }
    class var storeType : String {
        return NSStringFromClass(RESTIncrementalStore.self)
    }
    
    override func loadMetadata() throws {
        let uuid = NSProcessInfo.processInfo().globallyUniqueString
        self.metadata = [NSStoreTypeKey : RESTIncrementalStore.storeType, NSStoreUUIDKey : uuid]
    }
    
    override func executeRequest(request: NSPersistentStoreRequest, withContext context: NSManagedObjectContext?) throws -> AnyObject {
        
        switch (request.requestType) {
        case .FetchRequestType:
            print("Fetch request")
            return self.processFetchRequest(request as! NSFetchRequest, inContext: context!)
        case .SaveRequestType:
            print("Save request")
            self.processSaveRequest(request as! NSSaveChangesRequest)
        default:
            print("Other request, we are not supporting that!")
        }
        
        return []
    }
    
    override func obtainPermanentIDsForObjects(array: [NSManagedObject]) throws -> [NSManagedObjectID] {
        
        //if we wanna have the correct implementation we should go to the backend and create an empty object at this point, just to have an ID
        //instead of this, we just generate a guid, and forget about it.
        //Another possible solution whould be to store the ID on the object itself
        var idArray = [NSManagedObjectID]()
        
        array.forEach { (currentElement:NSManagedObject) -> () in
            
            let uuid = NSUUID().UUIDString
            let newPermanentID = self.newObjectIDForEntity(currentElement.entity, referenceObject: uuid)
            idArray.append(newPermanentID)
        }
        
        return idArray
    }

    override func newValuesForObjectWithID(objectID: NSManagedObjectID, withContext context: NSManagedObjectContext) throws -> NSIncrementalStoreNode {

        //always return an empty node
        let node = NSIncrementalStoreNode(objectID: objectID, withValues: [:], version: 1)
        return node
    }

////SALALALLALA: We are not supporting relationships
//    override func newValueForRelationship(relationship: NSRelationshipDescription, forObjectWithID objectID: NSManagedObjectID, withContext context: NSManagedObjectContext?) throws -> AnyObject {
//        
//    }
}

extension RESTIncrementalStore {
    
    func processSaveRequest(saveRequest:NSSaveChangesRequest) {
        
        //save all the freshly inserted entities. for this, we need to map them to a JSON object
        saveRequest.insertedObjects?.forEach({ (insertedObject : NSManagedObject) -> () in
            
            let json = insertedObject.JSONRepresentation();
            print("\(json)")
            
            let url = "http://esrap.herokuapp.com/kv/\(insertedObject.entity.name!)"
            
            Alamofire.request(.POST, url, parameters: [:], encoding: ParameterEncoding.Custom({ (convertible, params) -> (NSMutableURLRequest, NSError?) in
                let mutableRequest = convertible.URLRequest.copy() as! NSMutableURLRequest
                mutableRequest.HTTPBody = json.dataUsingEncoding(NSUTF8StringEncoding, allowLossyConversion: false)
                return (mutableRequest, nil)
            }), headers: ["Content-Type":"application/json"]).response { request, response, data, error in
                print("ResponseCode:\(response?.statusCode)")
            }
        })
    }
    
    func processFetchRequest(fetchRequest:NSFetchRequest, inContext context:NSManagedObjectContext) -> AnyObject {
        
        switch fetchRequest.resultType {
        case NSFetchRequestResultType.ManagedObjectResultType:
            
            //fetch all the entities with this name
            let url = "http://esrap.herokuapp.com/kv/\(fetchRequest.entity!.name!)"

            let content =  NSData.init(contentsOfURL: NSURL(string: url)!)
            
            var fetchedObjects = [NSManagedObject]()
            do {
                let itemList = try NSJSONSerialization.JSONObjectWithData(content!, options: []) as! NSArray
                
                itemList.forEach({ (currentItem) -> () in
                    
                    let currentID:String = currentItem["_id"] as! String
                    print("ID:\(currentID)")
                    
                    let objectID = self.newObjectIDForEntity(fetchRequest.entity!, referenceObject: currentID )
                    let object = context.objectWithID(objectID)
                    
                    //fill the attributes
                    fetchRequest.entity?.attributes().forEach({ (currentAttribute) -> () in
                        
                        let value = currentItem[currentAttribute.name]
                        object.setValue(value, forKey: currentAttribute.name)
                        
                    })
                    
                    fetchedObjects.append(object)
                })

            } catch {
                print("json error: \(error)")
            }
            
            return fetchedObjects
        default:
            //AAAAAND as usual, we are not supporting other request types
            return []
        }
        
    }

}
