//
//  AppDelegate.swift
//  IncrementaStoreTest
//
//  Created by balazsgollner on 19/10/15.
//  Copyright © 2015 EPAM. All rights reserved.
//

import UIKit
import CoreData

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate {

    var window: UIWindow?
    
    func application(application: UIApplication, didFinishLaunchingWithOptions launchOptions: [NSObject: AnyObject]?) -> Bool {
        // Override point for customization after application launch.
        return true
    }
}

